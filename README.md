# Shifter Sans

Sempre que mudamos o Shifter – e esta não é a primeira vez –, uma das primeiras coisas que fazemos é tentar perceber o seu aspecto futuro. Em _jams_ de design, criamos logos, imaginamos artigos e escolhemos novas tipografias.

No _redesign_ que lançámos em 2018 – e que nos deu boas lições para este que agora vamos fazer –, optámos pela **Libre Franklin**, uma fonte _libre_, para fonte principal – é a Libre Franklin que usamos no nosso logo actual, nos títulos dos artigos e nas peças de comunicação. Já para o texto do corpo dos artigos, e como segunda fonte no geral, escolhemos a **Open Sans**, um tipo de letra bastante conhecido, que se encontra na biblioteca Google Fonts.
**Libre Franklin** e **Open Sans** são o combo que temos usado até aqui, depois de antes termos navegado pela DIN e pela Helvetica, ambas fontes proprietárias. Escolhemos a Libre Franklin e a Open Sans por serem _open source_, uma filosofia que já no ano passado começámos a trazer para o Shifter e que agora é um dos verticais da mudança. Assim, decidimos voltar a olhar para o catálogo e trocámos a Open Sans pela **Work Sans**; contudo, para fonte principal e para reforçar o tom de voz próprio que queremos utilizar, decidimos criar uma própria. Chama-se **Shifter Sans** e está a ser desenhada pelo  [Ricardo Santos](https://dat-rs.com/) , com o **João Ribeiro** e o **Daniel Hoesen**; ainda não está terminada, faltando-lhe alguns caracteres, mas pretendemos disponibilizá-la de forma aberta para que qualquer pessoa possa usá-la para composições gráficas ou como base para a criação da sua própria fonte.

A **Shifter Sans** está presente transversalmente na campanha #Shifter2020 e vai ser a tipografia do nosso novo logo, que revelaremos em breve. Apesar da actualidade e qualidade da imagem presente do Shifter, a sua forma disruptiva e a sua dependência da cor tornam-na estática e pouco versátil – algo que não condiz com o espírito e os valores assumidos pelo Shifter ao longo do tempo. Mais do que uma imagem identificativa, queremos uma linguagem gráfica utilizável, que se transforme no tom gráfico do Shifter.

Assim, o novo grafismo do Shifter nasce a partir da criação da fonte **Shifter Sans**.

A ideia de que parte esta fonte é simples. Quisemos transmitir o espírito experimental do Shifter, optando por uma tipografia de desenho simples, ângulos rectos e que mesmo a preto permite qualquer pessoa perceber a sua lógica de construção. Shifter Sans é uma tipografia que não tem necessidade de surgir com o produto todo refinado, dá corpo às frases que definem a marca com atitude, rigor e clareza, deixando sempre um lado aberto à crítica e interação traduzido no seu desenho simples e elástico.

Texto publicado [aqui](https://shifter.sapo.pt/shifter2020/shifter-sans/).

## Licença

- Shifter Sans está licenciada segundo a v.1.1 da [Open Font License](http://scripts.sil.org/OFL), podendo ser utilizada de forma livre em qualquer projecto
- Podes saber mais sobre a licença no documento [OFL.txt](https://gitlab.com/shifter_pt/shifter-sans/blob/master/OFL.txt)
